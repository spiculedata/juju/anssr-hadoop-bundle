# Overview

At Spicule we believe in offering quality products, we also fully subscribe to the open source ethos and like to write software that is open and free to use. 
But we also believe in being able to provide first class support, quickly and easily. This is why we teamed up with Canonical to create the Anssr Data Platform.

Anssr allows users to spin up a large variety of data processing software at the click of a button, or one line in a terminal and scale that software just as easily. 
But if you use it in production and want support, you can get it just as easily by enabling the service level agreements in the platrform and in so doing gaining access 
to our wealth of technical knowledge along with Ubuntu operating system support to ensure your platforms continue running smoothly, regardless of workload.

## Components

This bundle will deploy 5 machines with the following components:

* Hadoop Namenode
* Hadoop Resource Manager
* Hadoop Worker(DataNode and NodeManager)
* Hadoop Client

# Verifying

The applications that make up this bundle provide status messages to indicate when they are ready:

    juju status

This is particularly useful when combined with watch to track the on-going progress of the deployment:

    watch -n 2 juju status

The message for each unit will provide information about that unit's state. Once they all indicate that they are ready, perform application smoke tests to verify that the bundle is working as expected.

# Scaling

To scale this deployment you can add more processing power by adding more Workers. 

For 1:

   juju add-unit worker

For multiple new units:
 
   juju add-unit -n <x> worker

You can also add more resource managers and namenodes:

    juju add-unit -n <x> namenode
    juju add-unit -n <x> resourcemanager

