Getting Started
===============

This bundle will deploy a fully operational Hadoop stack to your hardware.

The bundle will spin up 5 machines. Included in this bundle is:

* Hadoop Namenode
* Hadoop ResourceManager
* Hadoop Worker
* Hadoop Client
* Hadoop Plugin

Deploying
---------

To deploy click the green Deploy button and click through the various deployment target options.

Once up and running all units should be in an active state with a ready message.

Scaling
-------

To scale the cluster you can easiy add more worker nodes.

    juju add-unit -n <x> worker

where x is the number of units to add.

To scale other services just replace the word worker with the service you want to scale.

Exposing
--------

To open ports select the charm you want to expose and click on the Expose button in the top left. From there, select expose and deploy changes.

This will open a port on the firewall for external access to that service. 

Be aware if you're in a public cloud, by default it will expose the service to every incoming IP.
